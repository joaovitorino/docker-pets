https://gitlab.devops.somosagility.com.br/snippets/7


# Sobre
Projeto demonstrativo de criação de pipeline e IaC tendo como base o projeto [docker-pets](https://github.com/docker-archive/docker-pets/tree/1.0/web).



## [Requisitos](https://gitlab.devops.somosagility.com.br/snippets/7)

Automação: Uso de terraform e ansible para provisionamento da infra estrutura e deploy. 
Infraestrutura: 4 instancias na EC2 na amazon para o cluster swarm (1 master e 3 workers)
Container: Imagem docker utilizando cluster swarm 
CI: Gitlab


## Stages da CI

- test :
    Realiza os testes de lint, problemas de segurança no código (bandit), teste únitário
   
- build : Build da imagem docker


- infra: Utliza o terraform para criar instancias na AWS e cria um cluster swarm para deploy

- Deploy: Deploy da imagem criado no stage build no swarm criado no stage infra

## Requisitos

1 - Docker e docker-compose

2 -Variaveis de ambiente AWS_ACCESS_KEY_ID e AWS_SECRET_ACCESS_KEY *(Ignorar caso esteja executando a imagem localmente)*  usadas pelo terraform para criar instancias EC2 na AWS. 
No gitlab CI essas variaveis podem ser mascaradas para não mostrar o valor no log. Em ambiente local essas variaveis podem ser colocados no arquivo [secret.tfvars](https://learn.hashicorp.com/tutorials/terraform/aws-variables#from-a-file) (arquivo já ignorado pelo git) e executado da seguinte forma:

```
$ terraform apply -var-file=secret.tfvars 
```

3 - Ansible *(Ignorar caso esteja executando a imagem localmente e sem swarm)*

## Execução em ambiente local

#### Build imagem
```
$ git clone https://gitlab.com/joaovitorino/docker-pets.git
$ cd docker-pets/web
$ docker build -t docker-pets .
$ cd ../ docker-compose -d pets-dev.compose.yml
# Ou
$ docker run -it -p 5000:5000 docker-pets
```

## Execução na AWS
#### Criação ambiente
`Não necessaŕio caso faça deploy em outro ambiente`

```
$ git clone https://gitlab.com/joaovitorino/docker-pets.git
$ cd docker-pets/web
$ docker build -t docker-pets .
$ docker tag docker-pets $REGISTRY/docker-pets:<versao>
$ cd infra/terraform/
# Colocar as variveis AWS_ACCESS_KEY_ID e AWS_SECRET_ACCESS_KEY no arquivo secret.tfvars 
$ terraform init && terraform validate
$ terraform plan --out=saida.tf
$ terraform apply -state=estado.tf -auto-approve  saida.tf
```

Edite o arquivo infra/ansible/hosts com os ips das instancias criadas. Defina uma maquina como master. Utilize como base o arquivo [infra/ansible/hosts.sample](hosts.sample)

```
$ cd infra/ansible
$ ansible-playbook -i hosts deploy-service.yml
```
#### Deploy container AWS

- Metodo 1

Editar o arquivo [pets-container-compose.yml](pets-container-compose.yml) colocando em imagem o caminho da imaegm no registry (docker hub, gitlab ou registry privado)


  ```
  # Copiar os arquivos do compose para a instância master
  $ scp pets-container-compose.yml ec2_user@<instancia_aws>
  $ scp pets-prod-compose.yml ec2_user@<instancia_aws>
  # SSH em algum instancia da AWS. A chave ja foi configurado pelo terraform
  # scp 
  $ssh ec2_user@<instancia_aws>
  $ printf "senhadeumaseis" | docker secret create admin_password -
  $ docker network create -d overlay ucp-hrm
  $ docker stack deploy --compose-file pets-container-compose.yml --compose-file pets-prod-compose.yml
  ```

 - Metodo 2 ( Ver TODO)

 Edite o arquivo infra/ansible/hosts com os ips das instancias criadas. Defina uma maquina como master. Utilize como base o arquivo [infra/ansible/hosts.sample](hosts.sample)

 ```
$ cd infra/ansible
$ ansible-playbook -i hosts deploy-service.yml 
 ```

## TODO

* Criar ambiente de stage na AWS.

* Passar credenciais para login no registry pelo ansible.

* Testes com selenium em stage.

*  Desfazer com o terraform o ambiente criado.

[x] Configurar terraform para criar a chave